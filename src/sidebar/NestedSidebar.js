import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import axios from "axios";

import "./NestedSidebar.css";

import coursesStructure from "../resources/exampleMenuItems";

export default class NestedSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coursesStructure: coursesStructure,
      expandedCourse: "",
      expandedSection: "",
      expandedLesson: ""
    }

    this.onCourseItemClick = this.onCourseItemClick.bind(this);
    this.onSectionItemClick = this.onSectionItemClick.bind(this);
    this.onLessonItemClick = this.onLessonItemClick.bind(this);
  }

  componentDidMount() {
    axios.get("http://localhost:8080/courses/")
      .then(response => {
        let courses = response.data;
        const menuItems = this.extractMenuItems(courses);
        this.setState({
          coursesStructure: menuItems
        });
      });
  }

  extractMenuItems(courses) {
    let menuItems = [];
    for (let i = 0; i < courses.length; i++) {
      let course = courses[i];
      let courseItem = {
        title: course.title,
        shortTitle: course.shortTitle,
        sections: []
      }

      for (let j = 0; j < course.sections.length; j++) {
        let section = course.sections[j];
        let sectionItem = {
          title: section.title,
          shortTitle: section.shortTitle,
          lessons: []
        };

        for (let k = 0; k < section.lessons.length; k++) {
          let lesson = section.lessons[k];
          let lessonItem = {
            title: lesson.title,
            shortTitle: lesson.shortTitle,
            sublessons: []
          }
          lessonItem.sublessons.push({
            title: "Introduction",
            shortTitle: "intro"
          });

          for (let l = 0; l < lesson.sublessons.length; l++) {
            let sublesson = lesson.sublessons[i];
            let sublessonItem = {
              title: sublesson.title,
              shortTitle: sublesson.shortTitle
            };
            lessonItem.sublessons.push(sublessonItem);
          }
          lessonItem.sublessons.push({
            title: "Quiz",
            shortTitle: "quiz"
          });
          sectionItem.lessons.push(lessonItem);
        }
        courseItem.sections.push(sectionItem);
      }
      menuItems.push(courseItem);
    }
    return menuItems;
  }

  /**
   * 
   * @param {Event} event 
   */
  onCourseItemClick(event) {
    this.setState({
      expandedCourse: this.state.expandedCourse === event.target.value ? "" : event.target.value
    });
  }

  onSectionItemClick(event) {
    this.setState({
      expandedSection: this.state.expandedSection === event.target.value ? "" : event.target.value
    });
  }

  onLessonItemClick(event) {
    this.setState({
      expandedLesson: this.state.expandedLesson === event.target.value ? "" : event.target.value
    });
  }

  render() {
    const coursesStructure = this.state.coursesStructure;
    return (
      <div class="sidenav">
        {coursesStructure.map(course =>
          <div>

            <p className="nested-level-1" title={course.title}>{course.title}

              <button className="collapse-btn" value={course.shortTitle} onClick={this.onCourseItemClick}>{this.state.expandedCourse === course.shortTitle ? "-" : "+"}</button>
            </p>
            {this.state.expandedCourse === course.shortTitle ? course.sections.map(section =>

              <div>
                <p className="nested-level-2" title={section.title}>{section.title}
                  <button className="collapse-btn" value={section.shortTitle} onClick={this.onSectionItemClick}>{this.state.expandedSection === section.shortTitle ? "-" : "+"}</button>
                </p>
                {this.state.expandedSection === section.shortTitle ? section.lessons.map(lesson =>

                  <div>
                    <p className="nested-level-3" title={lesson.title}>{lesson.title}
                      <button className="collapse-btn" value={lesson.shortTitle} onClick={this.onLessonItemClick}>{this.state.expandedLesson === lesson.shortTitle ? "-" : "+"}</button>
                    </p>
                    {this.state.expandedLesson === lesson.shortTitle ? lesson.sublessons.map(sublesson =>

                      <p className="nested-level-4" title={sublesson.title}>
                        <NavLink to={`/courses2/${course.shortTitle}/${section.shortTitle}/${lesson.shortTitle}/${sublesson.shortTitle}/`}>
                          {sublesson.title}
                        </NavLink>
                      </p>
                    ) : null}
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>
        )}
        <p className="nested-level-1" title="Settings">
          <NavLink to="/settings">
            Settings
          </NavLink>
        </p>
      </div>
    )
  }
}
