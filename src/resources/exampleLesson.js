const sampleLesson = {
  title: "Solving One-Step Equations",
  introURL: "https://tutoring-thing.nyc3.digitaloceanspaces.com/sample-lesson/one-step-equations/Equations-Intro.mp4",
  learningLoop: [
    {
      title: "Solving Equations with Subtraction",
      subLessonData: [
        {
          errorType: 0,
          videoURL: "https://tutoring-thing.nyc3.digitaloceanspaces.com/sample-lesson/one-step-equations/equations-subtracting/Equations-Subtraction1.mp4",
          questionText: "What is the value of x in 'x-7=4'",
          correctChoice: "A",
          choices: [
            {
              letter: "A",
              choiceText: "x=11",
              errorType: 0
            },
            {
              letter: "B",
              choiceText: "x=-3",
              errorType: 1
            }
          ]
        },
        {
          errorType: 1,
          videoURL: "https://tutoring-thing.nyc3.digitaloceanspaces.com/sample-lesson/one-step-equations/equations-subtracting/Equations-Subtraction2.mp4",
          questionText: "What is the value of x in 'x-7=5'",
          correctChoice: "A",
          choices: [
            {
              letter: "A",
              choiceText: "x=12",
              errorType: 0
            },
            {
              letter: "B",
              choiceText: "x=-2",
              errorType: 1
            }
          ]
        },
      ]
    },
    {
      title: "Solving Equations with Addition",
      subLessonData: [
        {
          errorType: 0,
          videoURL: "https://tutoring-thing.nyc3.digitaloceanspaces.com/sample-lesson/one-step-equations/equations-adding/Equations-Addition1.mp4",
          questionText: "What is the value of x in 'x+7=11'",
          correctChoice: "A",
          choices: [
            {
              letter: "A",
              choiceText: "x=4",
              errorType: 0
            },
            {
              letter: "B",
              choiceText: "x=18",
              errorType: 1
            }
          ]
        },
        {
          errorType: 1,
          videoURL: "https://tutoring-thing.nyc3.digitaloceanspaces.com/sample-lesson/one-step-equations/equations-adding/Equations-Addition2.mp4",
          questionText: "What is the value of x in 'x+7=10'",
          correctChoice: "A",
          choices: [
            {
              letter: "A",
              choiceText: "x=3",
              errorType: 0
            },
            {
              letter: "B",
              choiceText: "x=17",
              errorType: 1
            }
          ]
        },
      ]
    }
  ],
  assessment: [
    {
      questionText: "What is the value of x in 'x-5=10'",
      choices: [
        {
          letter: "A",
          choiceText: "x=5"
        },
        {
          letter: "B",
          choiceText: "x=15"
        },
        {
          letter: "C",
          choiceText: "x=2"
        },
        {
          letter: "D",
          choiceText: "x=50"
        }
      ],
      correctChoice: "B",
      explanationURL: "",
      referenceURL: ""
    },
    {
      questionText: "What is the value of x in 'x+2=10'",
      choices: [
        {
          letter: "A",
          choiceText: "x=8"
        },
        {
          letter: "B",
          choiceText: "x=12"
        },
        {
          letter: "C",
          choiceText: "x=20"
        },
        {
          letter: "D",
          choiceText: "x=5"
        }
      ],
      correctChoice: "A",
      explanationURL: "",
      referenceURL: ""
    },
  ]
};

export default sampleLesson;