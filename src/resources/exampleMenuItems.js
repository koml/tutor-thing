const coursesStructure = [
  {
    title: "Algebra 1",
    shortTitle: "algebra-1",
    sections: [
      {
        title: "Solving Equations",
        shortTitle: "solving-equations",
        lessons: [
          {
            title: "One Step Equations",
            shortTitle: "one-step",
            sublessons: [
              {
                title: "Solving Equations with Addition",
                shortTitle: "addition"
              },
              {
                title: "Solving Equations with Subtraction",
                shortTitle: "subtraction"
              },
              {
                title: "Quiz",
                shortTitle: "quiz"
              }
            ]
          }
        ]
      },
      {
        title: "Graphing Equations",
        shortTitle: "graphing-equations",
        lessons: [
          {
            title: "Slope-intercept form",
            shortTitle: "slope-intercept",
            sublessons: []
          },
          {
            title: "Point slope form",
            shortTitle: "point-slope",
            sublessons: []
          }
        ]
      }
    ]
  }
]

export default coursesStructure;