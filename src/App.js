import React from 'react';
import LessonPlayer from './lessons';

import { Switch, Route, Router } from 'react-router-dom';
import LessonContainer from './lessons';
import HomePage from './HomePage';
import LessonPlayerContainer from "./lessons/LessonPlayerContainer";

import "./App.sass";

import NestedSidebar from "./sidebar";

const Routes = () => {
  return (

    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path='/courses/:course/:section/:lesson/:sublesson/' component={LessonContainer} />
      <Route path="/courses2/:course/:section/:lesson/:sublesson/" component={LessonPlayerContainer} />
    </Switch>

  )
}

class App extends React.Component {
  render() {
    return (

      <div>
        <NestedSidebar />
        <div className="main-content">
          <Switch className>
            <Route exact path="/" component={HomePage} />
            <Route path='/courses/:course/:section/:lesson/:sublesson/' component={LessonContainer} />
            <Route path="/courses2/:course/:section/:lesson/:sublesson/" component={LessonPlayerContainer} />
          </Switch>
        </div>
      </div>

    );
  }
}

export default App;
