import React, { Component } from 'react';

class NestedSideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuItems: props.menuItems,
      currentlyOpenKey: props.currentlyOpen
    };
  }

  render() {
    return (
      <div className="sidenav">
        <a href="#">Item1</a>
        <a href="#">Item2</a>
        <a href="#">Item3</a>
      </div>
    )
  }
}

export default NestedSideBar;
