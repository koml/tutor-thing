import React, { Component } from 'react';
import sampleLesson from '../resources/exampleLesson'

/**
 * Component that handles how a lesson is played and questions are displayed to the user.
 * Each lesson consists of 3 parts, the intro, the "learning loop", and the assessment.
 * There are 5 possible states to be in, described below
 *    (1) Intro
 *    (2) Learning Loop, showing a video
 *    (3) Learning Loop, showing a question
 *    (4) Assessment, showing a question
 *    (5) Assessment, showing an explanation video
 * Note that the Learning Loop and Assessment parts of the lesson consist of multiple video/question pairs.
 * Therefore, we might need to loop back to those states.
 * These states will be mentioned periodically throughout the code to illustrate the transitions between them.
 */
class LessonPlayer extends Component {
  constructor(props) {
    super(props);

    /**
     * Initialize the default state. This is state that does not depend on the value of the currentlyPlayed prop.
     */
    this.state = {
      lesson: props.lesson
    }

    /**
     * The majority of the state is concerned with managing which state the lesson is in.
     * The specific way to check each state is shown below
     *     (1) --- if (eintro.played === false)
     *     (2) --- if (learningLoop.played === false && showVideo === true)
     *     (3) --- if (learningLoop.played === false && showVideo === false)
     *     (4) --- if (assessment.played === false && showVideo === true)
     *     (5) --- if (assessment.played === false && showVideo === false)
     * We check state down the list (i.e. being in state (2) implies that intro.played === true).
     */
    this.state = {
      sampleLesson: props.lesson,   // Will get this from the database when that's set up
      lessonState: {                // Object to keep track of the current things that were played from the lesson.
        intro: {
          played: false,
        },
        learningLoop: {
          played: false,
          lessonIndex: 0
        },
        assessment: {
          played: false,
          questionIndex: 0
        }
      },
      showVideo: true,             // Tells whether we should show a video (if true) or a question (if false)
      videoSrc: sampleLesson.introURL,    // Tells what video should be shown (if needed)
      question: null,                     // Tells what question should be shown (if needed)
      currentAnswer: "",                  // If a question is shown, the current value of the user's response
      errorType: 0                        // The type of error made (0 for no error, >0 for error). Tells what alternate question to go to.
    };



    this.onAnswerChange = this.onAnswerChange.bind(this);
    this.getTitle = this.getTitle.bind(this);
    this.doStateTransition = this.doStateTransition.bind(this);
    this.showQuestion = this.showQuestion.bind(this);
    this.onVideoEnd = this.onVideoEnd.bind(this);
    this.onQuestionSubmit = this.onQuestionSubmit.bind(this);
  }

  /**
   * Determines which title to show based on the state of the lesson.
   */
  getTitle() {
    let title = "";
    if (this.state.lessonState.intro.played === false) {
      // In state (1), so we just need to display "Introduction" for the intro
      title = "Introduction";
    } else if (this.state.lessonState.learningLoop.played === false) {
      // In state (2) or (3), so we should display the sublesson title so the user knows what topic is being discussed.
      let lessonIndex = this.state.lessonState.learningLoop.lessonIndex;
      title = this.state.sampleLesson.learningLoop[lessonIndex].title;
    } else if (this.state.lessonState.assessment.played === false) {
      // In state (4) or (5), so we should display the question number.
      title = `Assessment Question #${this.state.lessonState.assessment.questionIndex + 1}`;
    }

    return title
  }

  /**
   * Invokes a transition from one state to another depending on the parameters passed in.
   * The parameters errorType and showVideo give the _desired_ new attributes after the state
   * transition (they don't describe the current state!).
   *
   * We need this so we can do one setState() operation instead of having multiple, which could 
   * be prone to "null"-type errors.
   * 
   * The various state transitions are described as they come up in the code below, but in general,
   * we look for the current state (using the methods described in the class constructor), and then 
   * process the next state requirements based on the parameters to the function.
   * 
   * @param {number} errorType    0 if we don't need to switch to an alternate question, >0 if we do need to (the parameter then tells WHERE to change the question to).
   * @param {boolean} showVideo   true if we want to show a video next, false if we want to show a question next.
   */
  doStateTransition(errorType, showVideo) {
    if (this.state.lessonState.intro.played === false) {
      // In state (1)
      // Intro somehow was not played yet, so load it in and show the video.
      this.setState({
        videoSrc: this.state.sampleLesson.introURL,
        showVideo: true
      });
    } else if (this.state.lessonState.learningLoop.played === false) {
      // In state (2) or (3).
      // Check if we need to update data for the video source or the question.
      if (showVideo === true) {
        // In state (3). We will either be at the start of a new video/question pair (with errorType = 0),
        // or errorType will be > 0, and we will have to load in an alternate explanation video.
        // In either case, the index of learning loop sublesson is already in the component state, so
        // we just need to do a search for the errorType question and load the video URL in.
        console.log("in state (3)");
        let lessonIndex = this.state.lessonState.learningLoop.lessonIndex;
        let sublesson = this.state.sampleLesson.learningLoop[lessonIndex];
        console.log(sublesson);
        let videoSrc = "";
        for (let i = 0; i < sublesson.subLessonData.length; i++) {
          if (sublesson.subLessonData[i].errorType === errorType) {
            videoSrc = sublesson.subLessonData[i].videoURL;
            break;
          }
        }
        this.setState({
          showVideo: showVideo,
          videoSrc: videoSrc,
          errorType: errorType
        });
      } else {
        // In state (2).
        console.log("in state (2)");
        let lessonIndex = this.state.lessonState.learningLoop.lessonIndex;
        let sublesson = this.state.sampleLesson.learningLoop[lessonIndex];
        let question = null;
        for (let i = 0; i < sublesson.subLessonData.length; i++) {
          if (sublesson.subLessonData[i].errorType === errorType) {
            question = sublesson.subLessonData[i];
          }
        }
        this.setState({
          showVideo: showVideo,
          question: question,
          errorType: errorType
        });
      }
    } else if (this.state.lessonState.assessment.played === false) {
      // In state (4) or (5).
      if (showVideo === false) {
        // In state (4). Want to show an assessment question here.
        console.log("in state (4)");
        let questionIndex = this.state.lessonState.assessment.questionIndex;
        let question = this.state.sampleLesson.assessment[questionIndex];
        this.setState({
          showVideo: showVideo,
          question: question
        });
      } else {
        // In state (5). Want to show the explanation video for the question
        // that is already loaded in.
        console.log("in state (5)");
        this.setState({
          showVideo: showVideo,
          videoSrc: this.state.question.explanationURL
        });
      }
    }
  }

  /**
   * 
   */
  showQuestion() {
    return (
      <div>
        <h3>{this.state.question.questionText}</h3>
        {this.state.question.choices.map((item) =>
          <div>
            <label>
              <input type="radio" name="answer" value={item.letter} onChange={this.onAnswerChange} />
              {item.letter}: {item.choiceText}
            </label>
            <br />
          </div>
        )}
        <button onClick={this.onQuestionSubmit}>Submit</button>
      </div>
    )
  }

  /**
   * 
   * @param {Event} event 
   */
  onAnswerChange(event) {
    this.setState({
      currentAnswer: event.target.value
    });
  }

  /**
   * 
   */
  onVideoEnd() {
    if (this.state.lessonState.intro.played === false) {
      this.setState({
        lessonState: {
          intro: {
            played: true
          },
          learningLoop: this.state.lessonState.learningLoop,
          assessment: this.state.lessonState.assessment
        }
      }, () => this.doStateTransition(0, true));
    } else if (this.state.lessonState.learningLoop.played === false) {
      this.doStateTransition(this.state.errorType, false);
    } else if (this.state.lessonState.assessment.played === false) {
      // After an explanation video is done in the assessment section, 
      // we load in the next question.
      this.setState({
        lessonState: {
          intro: this.state.lessonState.intro,
          learningLoop: this.state.lessonState.learningLoop,
          assessment: {
            played: false,
            questionIndex: this.state.lessonState.assessment.questionIndex + 1
          }
        }
      }, () => this.doStateTransition(0, false));
    }
  }

  /**
   * Checks if the answer to the question is correct.
   * If the answer is correct, it invokes a state transition to the next video/question pair,
   * which can either be in state (2) or (4).
   * If the answer is incorrect, it invokes a state transition to the alternate video/question
   * pair corresponding to the type of error made.
   * @param {Event} event 
   */
  onQuestionSubmit(event) {
    const answerChoice = this.state.currentAnswer;
    if (this.state.lessonState.learningLoop.played === false) {
      if (this.state.question.correctChoice === answerChoice) {
        console.log("correct");
        if (this.state.lessonState.learningLoop.lessonIndex + 1 === this.state.sampleLesson.learningLoop.length) {
          // We are out of new questions to load in, so we should load in the assessment part of the lesson.
          this.setState({
            lessonState: {
              intro: this.state.lessonState.intro,
              learningLoop: {
                played: true,
                lessonIndex: this.state.sampleLesson.learningLoop.length
              },
              assessment: this.state.lessonState.assessment
            }
          }, () => this.doStateTransition(0, false));
        } else {
          this.setState({
            lessonState: {
              intro: this.state.lessonState.intro,
              learningLoop: {
                played: false,
                lessonIndex: this.state.lessonState.learningLoop.lessonIndex + 1
              },
              assessment: this.state.lessonState.assessment
            }
          }, () => this.doStateTransition(0, true));
        }
      } else {
        console.log("incorrect");
        let errorType = 0;
        let question = this.state.question
        for (let i = 0; i < question.choices.length; i++) {
          if (question.choices[i].letter === answerChoice) {
            errorType = question.choices[i].errorType;
            break;
          }
        }
        this.doStateTransition(errorType, true);
      }
    } else if (this.state.lessonState.assessment.played === false) {
      // We are in state (4). Check if the question was gotten right.
      // If it was, then we can load in the next question.
      // If the question was answered incorrectly, load in the explanation video.
      if (this.state.question.correctChoice === answerChoice) {
        // Question was answered correctly.
        this.setState({
          lessonState: {
            intro: this.state.lessonState.intro,
            learningLoop: this.state.lessonState.learningLoop,
            assessment: {
              played: false,
              questionIndex: this.state.lessonState.assessment.questionIndex + 1
            }
          }
        }, () => this.doStateTransition(0, false));
      } else {
        // Question was answered incorrectly.
        this.doStateTransition(0, true);
      }
    }
  }

  render() {
    console.log(this.state.sampleLesson);
    return (
      <div>
        <h1>{this.state.sampleLesson.title}</h1>
        <h2>{this.getTitle()}</h2>
        {this.state.showVideo === true ?
          <video width="40%" height="30%" onEnded={this.onVideoEnd} key={this.state.videoSrc} controls>
            <source src={this.state.videoSrc} />
          </video>
          : this.showQuestion()
        }
      </div>
    )
  }
}

export default LessonPlayer;