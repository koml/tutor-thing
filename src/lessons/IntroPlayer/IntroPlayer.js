import React, { Component } from 'react'


class IntroPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      introUrl: this.props.introUrl
    }
  }

  componentDidMount() {
    document.getElementById("intro-video").load();
  }

  render() {
    return (
      <div>
        <video id="intro-video" width="40%" height="30%" onEnded={this.props.onEnded} key={this.state.introUrl} controls>
          <source src={this.state.introUrl} />
        </video>
      </div>
    )
  }
}

export default IntroPlayer;