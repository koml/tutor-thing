import React, { Component } from 'react';

const VideoPlayer = (props) =>
  <video onEnded={props.onEnded} key={props.videoUrl} id="learning-loop-player" controls>
    <source src={props.vqPair.videoUrl} />
  </video>


export default VideoPlayer;