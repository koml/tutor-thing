import React, { Component } from 'react';

import VideoPlayer from "./VideoPlayer";
import QuestionViewer from "./QuestionViewer";

class LearningLoopPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lesson: props.lesson,
      sublessonTitle: props.sublesson,
      sublesson: {},
      showSublesson: false,
      vqPair: {},
      showVideo: true
    };

    this.updateVQPairByErrorType = this.updateVQPairByErrorType.bind(this);
    this.onVideoEnded = this.onVideoEnded.bind(this);
    this.onQuestionAnswered = this.onQuestionAnswered.bind(this);
    this.getNextSublesson = this.getNextSublesson.bind(this);
  }

  componentDidMount() {
    const sublessonTitle = this.state.sublessonTitle;
    const lesson = this.state.lesson;
    let foundSublesson = null;
    console.log(lesson.sublessons);
    for (let i = 0; i < lesson.sublessons.length; i++) {
      if (lesson.sublessons[i].shortTitle === sublessonTitle) {
        foundSublesson = lesson.sublessons[i];
        break;
      }
    }
    console.log(foundSublesson);
    this.setState({
      sublesson: foundSublesson
    }, () => this.updateVQPairByErrorType(0));
  }

  componentWillReceiveProps(nextProps) {
    console.log("LL player new props");
    const sublessonTitle = nextProps.sublesson;
    const lesson = nextProps.lesson;
    let foundSublesson = null;
    console.log(lesson.sublessons);
    for (let i = 0; i < lesson.sublessons.length; i++) {
      if (lesson.sublessons[i].shortTitle === sublessonTitle) {
        foundSublesson = lesson.sublessons[i];
        break;
      }
    }
    console.log(foundSublesson);
    this.setState({
      sublessonTitle: sublessonTitle,
      lesson: lesson,
      sublesson: foundSublesson,
      showVideo: true
    }, () => this.updateVQPairByErrorType(0));
  }


  /**
   * Updates the VQPair in the state to reflect the errorType given as a parameter.
   * 
   * @param {number} errorType 
   */
  updateVQPairByErrorType(errorType) {
    console.log("Updating VQPair");
    const vqPairs = this.state.sublesson.videoQuestionPairs;
    let vqPair = {};
    for (let i = 0; i < vqPairs.length; i++) {
      if (vqPairs[i].errorType === errorType) {
        vqPair = vqPairs[i];
        break;
      }
    }
    this.setState({
      vqPair: vqPair,
      showSublesson: true
    }, () => {
      document.getElementById("learning-loop-player").load();
    });

  }

  /**
   * Triggers a render of the QuestionViewer for the video after a video ends.
   */
  onVideoEnded() {
    this.setState({
      showVideo: false
    });
  }

  /**
   * Checks to see if the answer to the question is correct.
   * If it is, then it moves to the next sublesson by passing a message back to the LessonPlayerContainer.
   * If it is incorrect, then it updates the current VQPair using the errorType of the incorrect answer,
   * and then it re-renders the VideoPlayer.
   * @param {String} answerChoice
   */
  onQuestionAnswered(answerChoice) {
    if (this.state.vqPair.correctChoice === answerChoice) {
      // Correct answer, so move onto the next sublesson.
      this.props.onEnded(this.getNextSublesson());
    } else {
      // Look up the corresponding errorType from the answer choice.
      // Then load the vqPair with that errorType in, and play the video.
      let vqPair = this.state.vqPair;
      for (let i = 0; i < vqPair.videoQuestionChoices.length; i++) {
        if (vqPair.videoQuestionChoices[i].letter === answerChoice) {
          let errorType = vqPair.videoQuestionChoices[i].errorType;
          for (let j = 0; j < this.state.sublesson.videoQuestionPairs.length; j++) {
            if (this.state.sublesson.videoQuestionPairs[i].errorType === errorType) {
              this.setState({
                vqPair: this.state.sublesson.videoQuestionPairs[i],
                showVideo: true
              });
            }
          }
        }
      }
    }
  }

  getNextSublesson() {
    const sublessonTitle = this.state.sublessonTitle;
    const sublessonList = this.state.lesson.sublessons;
    for (let i = 0; i < sublessonList.length; i++) {
      if (sublessonList[i].shortTitle === sublessonTitle) {
        if (i === sublessonList.length - 1) {
          // If we are at the last sublesson, go to the quiz.
          return "quiz";
        }
        return sublessonList[i + 1].shortTitle;
      }
    }
  }

  render() {
    return (
      <div>
        <h1>Learning loop player</h1>
        {this.state.showSublesson ?
          this.state.showVideo ?
            <VideoPlayer
              vqPair={this.state.vqPair}
              onEnded={this.onVideoEnded}
            /> :
            <QuestionViewer
              vqPair={this.state.vqPair}
              onAnswered={this.onQuestionAnswered}
            /> :
          null
        }
      </div>
    )
  }
}

export default LearningLoopPlayer;