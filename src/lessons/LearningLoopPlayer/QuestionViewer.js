import React, { Component } from 'react'

class QuestionViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentAnswerChoice: ""
    };

    this.onAnswerChange = this.onAnswerChange.bind(this);
    this.onQuestionSubmit = this.onQuestionSubmit.bind(this);
  }

  /**
   * Updates the state to reflect the currently selected answer.
   * @param {Event} event 
   */
  onAnswerChange(event) {
    this.setState({
      currentAnswerChoice: event.target.value
    });
  }

  onQuestionSubmit() {
    this.props.onAnswered(this.state.currentAnswerChoice);
  }

  render() {
    return (
      <div>
        <h3>{this.props.vqPair.questionText}</h3>
        {this.props.vqPair.videoQuestionChoices.map(choice =>
          <div>
            <label>
              <input type="radio" name="answer" value={choice.letter} onChange={this.onAnswerChange} />
              {choice.letter}: {choice.choiceText}
            </label>
            <br />
          </div>
        )}
        <button onClick={this.onQuestionSubmit}>Submit</button>
      </div>
    )
  }
}

export default QuestionViewer;