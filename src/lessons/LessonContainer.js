import React, { Component } from 'react'

import LessonPlayer from './LessonPlayer';
import LessonSidebar from './LessonSidebar';
import sampleLesson from '../resources/exampleLesson';

import menuItems from '../resources/exampleMenuItems';


class LessonContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuItems: menuItems,
      currentlyPlaying: 0,
      currentLesson: sampleLesson
    };

    this.onClickNewSublesson = this.onClickNewSublesson.bind(this);
  }


  /**
   * 
   * @param {Event} event 
   */
  onClickNewSublesson(event) {
    this.setState({
      currentlyPlaying: event.target.key,
    });
  }

  render() {
    return (
      <div>
        <LessonSidebar menuItems={this.state.menuItems} lesson={this.state.currentLesson} onChange={this.onClickNewSublesson} />
        <LessonPlayer lesson={this.state.currentLesson} currentlyPlaying={this.state.currentlyPlaying} />
      </div>
    )
  }
}

export default LessonContainer