import React, { Component } from 'react';

import axios from "axios";

import IntroPlayer from "../IntroPlayer";
import LearningLoopPlayer from "../LearningLoopPlayer";
import QuizPlayer from "../QuizPlayer";

import { Redirect } from "react-router-dom";

class LessonPlayerContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      course: props.match.params.course,
      section: props.match.params.section,
      lesson: props.match.params.lesson,
      sublesson: props.match.params.sublesson,
      load: false,

      lessonData: {},
      nextPath: "",
      navigate: false
    };

    this.onIntroEnded = this.onIntroEnded.bind(this);
    this.onLearningLoopEnded = this.onLearningLoopEnded.bind(this);
    this.onAssessmentEnded = this.onAssessmentEnded.bind(this);
  }

  componentDidMount() {
    const course = this.props.match.params.course;
    const section = this.props.match.params.section;
    const lesson = this.props.match.params.lesson;
    const sublesson = this.props.match.params.sublesson;
    axios({
      method: "get",
      url: `http://localhost:8080/courses/name/${course}/sections/name/${section}/lessons/name/${lesson}`
    })
      .then(response => {
        console.log(response.data);
        this.setState({ lessonData: response.data, load: true });
      });
  }

  componentWillReceiveProps(nextProps) {
    console.log("component will receive props");
    if (JSON.stringify(nextProps.location.pathname) !== JSON.stringify(this.props.location.pathname)) {
      console.log("not same props");
      this.setState({
        course: nextProps.match.params.course,
        section: nextProps.match.params.section,
        lesson: nextProps.match.params.lesson,
        sublesson: nextProps.match.params.sublesson,
        nextPath: "",
        navigate: false
      }, () => {
        axios({
          method: "get",
          url: `http://localhost:8080/courses/name/${this.state.course}/sections/name/${this.state.section}/lessons/name/${this.state.lesson}`
        })
          .then(response => {
            console.log(response.data);
            this.setState({ lessonData: response.data, load: true });
          });
      })
    }
  }

  onIntroEnded() {
    const course = this.state.course;
    const section = this.state.section;
    const lesson = this.state.lesson;
    const sublesson = this.state.lessonData.sublessons[0].shortTitle;
    const newPath = `/courses2/${course}/${section}/${lesson}/${sublesson}/`;
    console.log("next path: " + newPath);
    this.setState({
      newPath: newPath,
      navigate: true
    });
  }

  onLearningLoopEnded(nextSublesson) {
    console.log("learning loop ended, next sublesson: " + nextSublesson);
    const course = this.state.course;
    const section = this.state.section;
    const lesson = this.state.lesson;
    const sublesson = nextSublesson;
    const newPath = `/courses2/${course}/${section}/${lesson}/${sublesson}/`;
    this.setState({
      newPath: newPath,
      navigate: true
    });
  }

  onAssessmentEnded() {

  }

  render() {
    return (
      <div key={this.props.match.sublesson}>
        {this.state.load ?
          this.state.navigate ?
            <Redirect to={this.state.newPath} /> :
            this.state.sublesson === "intro" ?
              <IntroPlayer
                introUrl={this.state.lessonData.introUrl}
                onEnded={this.onIntroEnded}
              /> :
              this.state.sublesson === "quiz" ?
                <QuizPlayer
                  lesson={this.state.lessonData}
                  onEnded={this.onAssessmentEnded}
                /> :
                <LearningLoopPlayer
                  lesson={this.state.lessonData}
                  sublesson={this.state.sublesson}
                  onEnded={this.onLearningLoopEnded}
                />
          :
          null
        }
      </div>
    )
  }
}

export default LessonPlayerContainer;