import React, { Component } from 'react'
import QuestionViewer from './QuestionViewer';
import VideoPlayer from "./VideoPlayer"

class AssessmentPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: props.lesson.quizQuestions,
      currentQuestionNumber: 0,  // Zero-indexed for easy list access.
      currentAnswerChoice: "",
      showVideo: false,
      quizCompleted: false
    };
    this.onQuestionSubmit = this.onQuestionSubmit.bind(this);
    this.onVideoEnded = this.onVideoEnded.bind(this);
    this.incrementQuestion = this.incrementQuestion.bind(this);
  }


  onQuestionSubmit(answerChoice) {
    const currentQuestion = this.state.questions[this.state.currentQuestionNumber];
    if (answerChoice === currentQuestion.correctChoice) {
      // Correct answer, so we can increment to the next question.
      this.incrementQuestion();
    } else {
      this.setState({
        showVideo: true
      });
    }
  }

  onVideoEnded() {
    this.incrementQuestion();
  }

  /**
   * Moves forward one question index, or if there are no more questions,
   * sets the completed state flag to "true".
   */
  incrementQuestion() {
    const currentQuestionNumber = this.state.currentQuestionNumber;
    if (currentQuestionNumber === this.state.questions.length - 1) {
      console.log("Done with quiz");
      this.setState({
        quizCompleted: true,
        showVideo: false
      });
    } else {
      this.setState({
        currentQuestionNumber: currentQuestionNumber + 1,
        showVideo: false
      });
    }
  }

  render() {
    const quizQuestion = this.state.questions[this.state.currentQuestionNumber];
    return (
      <div>
        {this.state.quizCompleted ?
          <h1>Congratulations! You finished the lesson!</h1> :
          this.state.showVideo ?
            <VideoPlayer videoUrl={quizQuestion.explanationUrl} onEnded={this.onVideoEnded} /> :
            <QuestionViewer question={quizQuestion} questionNumber={this.state.currentQuestionNumber} onSubmit={this.onQuestionSubmit} />
        }
      </div>
    )
  }
}

export default AssessmentPlayer;