import React from 'react'

const VideoPlayer = (props) =>
  <video onEnded={props.onEnded} key={props.videoUrl} controls>
    <source src={props.videoUrl} />
  </video>

export default VideoPlayer;