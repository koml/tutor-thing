import React, { Component } from 'react'

export default class componentName extends Component {
  constructor(props) {
    super(props);
    let question = props.question;

    // Sort answer choices just in case they come in out of order.
    question.quizChoices.sort((a, b) => {
      return a.letter.localeCompare(b.letter);
    });

    this.state = {
      currentAnswerChoice: "",
      question: question
    }

    this.onAnswerChange = this.onAnswerChange.bind(this);
    this.onQuestionSubmit = this.onQuestionSubmit.bind(this);
  }

  onAnswerChange(event) {
    this.setState({
      currentAnswerChoice: event.target.value
    });
  }

  onQuestionSubmit() {
    this.props.onSubmit(this.state.currentAnswerChoice);
  }

  render() {
    const quizQuestion = this.state.question;
    return (
      <div>
        <h1>Assessment Player</h1>
        <h2>Question #{this.props.questionNumber + 1} </h2>
        <h3>{quizQuestion.questionText}</h3>
        {quizQuestion.quizChoices.map(choice =>
          <div>
            <label>
              <input type="radio" name="answer" value={choice.letter} onChange={this.onAnswerChange} />
              {choice.letter}: {choice.choiceText}
            </label>
            <br />
          </div>
        )}
        <button onClick={this.onQuestionSubmit}>Submit</button>
      </div>
    )
  }
}
